# Intro
This project is named **eKitchen** which is targeted to provide automation for creating and maintaining Escenic server. The idea is to maintain developer environment based on **recipe** which describes artifacts and few defintions to make a Escenic dev environment ready or change it.

This automation is basically done using [Ansible](http://ansible.com).
### Kithen
Kitchen is mainly the builder machine which is able to create deployable artifacts to server(e.g. engine.ear) and deploy it to *ecehost* machine.

### ecehost
Here, **ecehost** is refered a machine which has standard Escenic deployment environment. A single *kitchen* can serve multiple *ecehost* machine. It is also possible(and more likely) that ecehost has it's own *kitchen* inside.
# Installation
### Requirements

 - Ubuntu 12.04+ 
 - Download setup scripts found in https://bitbucket.org/nahian/ansibles/ project in **ece/contrib/** location.

### Setup Kitchen
```
$ ./setup-requisites.yml
$ ./setup-kitchen.yml
```

### Setup ecehost with kitchen
```
$ ./setup-requisites.yml
$ ./setup-fullstack.yml <branch> <recipe>
```
`branch` specifies the branch name to sync ansible playbooks and recipes
# Using kitchen
Make sure that your kitchen machine host and ecehost is properly configured in */etc/ansible/hosts* file.

## Cooking
Any of the following command will do the same which are 
- Download Engine and plug-in artifacts specified in recipe from repository  
- Create publication properties specified in recipe
- Build *engine.ear*
- Archieve *engine.ear* and other deployable artifacts for specified recipe

```
$ cook -e recipe=default
$ cook 
$ chef -t cook
$ ansible-playbook chef.yml -t cook
```
Here, `cook` and `chef` are just aliases and `default` is used by default if no recipe is specified.

## Serving
Any of the following command will do the same which are 
- Deploy archieved engine.ear 
- Deploy archieved solr config
- Restart ece server
- Update publication resources

```
$ serve -e "recipe=default diner=<ecehost>"
$ serve -e diner=<ecehost>
$ chef -t serve -e diner=<ecehost>
$ ansible-playbook chef.yml -t serve -e diner=<ecehost>
```
Here, `serve` is an aliases. The variable `diner` is mandatory for this command.

**Cooking & Serving in a single command**
```
$ chef -e "recipe=default diner=<ecehost>"
```

## Changes

###  Kitchen 1.1
 - setup-ecehost.yml playbook for setting up a standard Escenic host using a recipe
 - both setup-ecehost.yml and setup-kitchen.yml playbooks should be run using root user(not escenic user).
 - publication defintions are to be defined in versions.yml in recipe
 - usefull scripts in contrib/ folder to fully setup ecehost+kitchen on an vanilla ubuntu 12.04+ VM image.
 - fixed a problem downloading non maven3 style snapshot artifacts.
 
###  Kichen 1.0
 - chef.yml creates engine.ear based on recipe
 - chef.yml deployes engine.ear and solr config
 - setup-kitchen.yml playbook for installing a kitchen on a Ubuntu 12.04+ machine
