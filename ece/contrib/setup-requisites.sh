#!/usr/bin/env bash

work=/opt/ekitchen

# add required packages
apt-get update
apt-get install -y python-software-properties
apt-add-repository -y ppa:rquillo/ansible
apt-get update
apt-get install -y ansible sshpass git

# Create inventory file for ansible
echo "localhost ansible_connection=local

[kitchen]
localhost ansible_connection=local

[ecehost]
# <your_ece_host> ansible_connection=ssh ansible_ssh_user=<user> ansible_ssh_pass=<pass>
# you may want to use ssh key based login instead of user pass" > /etc/ansible/hosts

# download required playbook
test -d $work || ( mkdir -p $work && chmod 777 $work)
cd $work
git clone https://nahian@bitbucket.org/nahian/ansibles.git || echo "Already cloned ansible project"