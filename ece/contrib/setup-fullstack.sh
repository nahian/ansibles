#!/usr/bin/env bash

work=/opt/ekitchen

test $# -lt 2 && echo "Missing paramter! Usage: '$0 <branch> <recipe>'" && exit 1
branch=$1
recipe=$2

echo "Using Escenic Kitchen branch: $branch"
echo "Using recipe: $recipe"

cd $work/ansibles
git pull -u origin $branch
git checkout $branch

#copy custom recipes
test -d /tmp/recipes && cp -r /tmp/recipes/* $work/ansibles/ece/recipes && rm -rf /tmp/recipes

# run builder setup playbook
cd $work/ansibles/ece
ansible-playbook setup-ecehost.yml -e "diner=localhost recipe=$recipe" && \
ansible-playbook setup-kitchen.yml --skip-tags=aptupdate && \
su -c "ansible-playbook chef.yml -e 'diner=localhost recipe=$recipe'" escenic