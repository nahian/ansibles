#!/usr/bin/env bash

work=/opt/ekitchen

test $# -lt 1 && echo "Missing paramter! Usage: '$0 <branch>'" && exit 1
branch=$1

echo "Using Escenic Kitchen branch: $branch"

cd $work/ansibles
git pull -u origin $branch
git checkout $branch


# run Kitchen setup playbook
cd $work/ansibles/ece
ansible-playbook setup-kitchen.yml --skip-tags=aptupdate
